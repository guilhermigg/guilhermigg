## About me
```json
{
    "🏷️ name":     "Guilherme Evaristo",
    "🇧🇷 country":  "Brasil",
    "📧 email":    "guilhermigg.e@gmail.com",
    "💼 linkedin": "/in/guilherme-e-288b1922a",
    "🐙 github":   "/guilhermigg"
}
```

